document.addEventListener('DOMContentLoaded', function () {
    const clienteForm = document.getElementById('clienteForm');
    const mensaje = document.getElementById('mensaje');
    const clientes = [];

    clienteForm.addEventListener('submit', function (e) {
        e.preventDefault();

        const nombre = document.getElementById('nombre').value;
        const email = document.getElementById('email').value;
        const telefono = document.getElementById('telefono').value;
        const cedula = document.getElementById('cedula').value;
        const ciudad = document.getElementById('ciudad').value;

        if (validarDatos(nombre, email, telefono, cedula, ciudad)) {
            const cliente = {
                nombre,
                email,
                telefono,
                cedula,
                ciudad
            };
        
            clientes.push(cliente);
        
            mostrarMensaje('Cliente registrado con éxito.', 'success');
            clienteForm.reset();
        } else {
            mostrarMensaje('Por favor, complete todos los campos correctamente.', 'error');
        }
    });

    function validarDatos(nombre, email, telefono, cedula, ciudad) {
        return (
            nombre.trim() !== '' &&
            email.trim() !== '' &&
            telefono.trim() !== '' &&
            cedula.trim() !== '' &&
            ciudad.trim() !== ''
        );

    function mostrarMensaje(texto, tipo) {
        mensaje.textContent = texto;
        mensaje.className = tipo;
        setTimeout(function () {
            mensaje.textContent = '';
            mensaje.className = '';
        }, 3000);
    }
});

